# Flask websocket

```
virtualenv venv -p python3
source venv/bin/activate
pip install -r requirements.txt
```

Run:

```
gunicorn --bind 0.0.0.0:5000 --workers 4 --threads 100 app:app
```

Or if we use the wsgi.py (which is used by alwaysdata.com)

```
gunicorn --bind 0.0.0.0:5000 --workers 4 --threads 100 wsgi:app
```
## Resources

* https://blog.miguelgrinberg.com/post/add-a-websocket-route-to-your-flask-2-x-application
* https://kevalnagda.github.io/flask-app-with-wsgi-and-nginx
